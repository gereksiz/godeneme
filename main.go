package main

import "github.com/gin-gonic/gin"

func main() {
	type Road struct {
		Name   string
		Number int
	}
	roads := []Road{
		{"Diamond Fork", 29},
		{"Sheep Creek", 51},
	}
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.JSON(200, roads)
	})

	router.Run(":28003")
}
